﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TestGame : MonoBehaviour
{
    public static bool GameOver {get; set;} = false;

    public static bool OpponentConnected {get; set;} = false;

    private Popup WaitingOverlay {get; set;}
    private float TimeWaitingOnOpponent {get; set;} = 0f;

    void Start()
    {
        WaitingOverlay = GameObject.Find("Waiting Overlay").GetComponent<Popup>();
        Network.Instance.Send("#l" + Network.PackString(GameState.MatchId, "M"));
    }

    void Update()
    {
        if (!OpponentConnected)
        {
            TimeWaitingOnOpponent += Time.deltaTime;
            if (TimeWaitingOnOpponent >= 30f)
            {
                Abort();
                TimeWaitingOnOpponent = 0f;
            }
        }
        if (OpponentConnected)
        {
            WaitingOverlay.Hide();
            TimeWaitingOnOpponent = 0f;
        }
        else
        {
            WaitingOverlay.Display();
        }

        if (GameOver)
        {
            GameOver = false;
            SceneManager.LoadScene("Lobby");
        }
    }

    public void Quit()
    {
        Network.Instance.Send("#q" + Network.PackString(GameState.MatchId, "M"));
        GameOver = true;
    }

    public void Abort()
    {
        Network.Instance.Send("#a" + Network.PackString(GameState.MatchId, "M"));
    }
}
