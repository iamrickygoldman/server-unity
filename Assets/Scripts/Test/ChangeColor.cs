﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeColor : MonoBehaviour
{
    public byte red;
    public byte green;
    public byte blue;

    void Start()
    {
        GetComponent<Button>().onClick.AddListener(delegate{Color();});
    }

    public void Color()
    {
        string text = "!t" + Network.PackString(GameState.MatchId, "M") + Network.PackString(red, "R") + Network.PackString(green, "G") + Network.PackString(blue, "B");
        Network.Instance.Send(text);
        TestBox.QueueChange = text;
    }
}
