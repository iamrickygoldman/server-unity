﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestBox : MonoBehaviour
{
    public static string QueueChange {get; set;} = "";

    void Update()
    {
        if (QueueChange != "")
        {
            byte red = (byte) Network.ExtractInt(QueueChange, "R");
            byte green = (byte) Network.ExtractInt(QueueChange, "G");
            byte blue = (byte) Network.ExtractInt(QueueChange, "B");
            QueueChange = "";
            GetComponent<Image>().color = new Color32(red,green,blue,255);
        }
    }
}
