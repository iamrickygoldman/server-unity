﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Enemies : MonoBehaviour
{
    public GameObject enemiesManagerObject;

    private EnemiesManager EnemiesManager {get; set;}

    void Awake()
    {
        EnemiesManager.ReloadEnemies = true;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            EnemiesManager.Block();
        }
    }
}
