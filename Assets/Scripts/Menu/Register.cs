﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class Register : MonoBehaviour
{
    public GameObject authManager;

    private TMP_InputField UsernameText {get; set;}
    private TMP_InputField EmailText {get; set;}
    private TMP_InputField PasswordText {get; set;}
    private TMP_InputField ConfirmText {get; set;}
    
    void Start()
    {
        authManager.GetComponent<AuthManager>().LoginToken();

        UsernameText = GameObject.Find("Username").GetComponent<TMP_InputField>();
        EmailText = GameObject.Find("Email").GetComponent<TMP_InputField>();
        PasswordText = GameObject.Find("Password").GetComponent<TMP_InputField>();
        ConfirmText = GameObject.Find("Confirm Password").GetComponent<TMP_InputField>();
        
        UsernameText.ActivateInputField();
    }

    void Update()
    {
        bool isShiftDown = Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);
        if (UsernameText.isFocused && Input.GetKeyDown(KeyCode.Tab) && !isShiftDown)
        {
            EmailText.ActivateInputField();
        }
        if (EmailText.isFocused && Input.GetKeyDown(KeyCode.Tab) && !isShiftDown)
        {
            PasswordText.ActivateInputField();
        }
        if (EmailText.isFocused && Input.GetKeyDown(KeyCode.Tab) && isShiftDown)
        {
            UsernameText.ActivateInputField();
        }
        if (PasswordText.isFocused && Input.GetKeyDown(KeyCode.Tab) && !isShiftDown)
        {
            ConfirmText.ActivateInputField();
        }
        if (PasswordText.isFocused && Input.GetKeyDown(KeyCode.Tab) && isShiftDown)
        {
            EmailText.ActivateInputField();
        }
        if (ConfirmText.isFocused && Input.GetKeyDown(KeyCode.Tab) && isShiftDown)
        {
            PasswordText.ActivateInputField();
        }
        if (Input.GetKeyDown(KeyCode.Return))
        {
            authManager.GetComponent<AuthManager>().Register();
        }
    }

    public void GoLogin()
    {
        SceneManager.LoadScene("Login");
    }
}
