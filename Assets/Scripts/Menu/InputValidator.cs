﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class InputValidator : MonoBehaviour
{
    private TMP_InputField Field {get; set;}
    
    void Start()
    {
        Field = GetComponent<TMP_InputField>();
    }

    // Update is called once per frame
    void Update()
    {
        bool isShiftDown = Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);
        if (Field.isFocused && Input.GetKeyDown(KeyCode.BackQuote) && !isShiftDown)
        {
            Field.text = Field.text.Remove(Field.text.Length - 1);
        }
    }
}
