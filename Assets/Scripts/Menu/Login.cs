﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class Login : MonoBehaviour
{
    public GameObject authManager;

    private TMP_InputField UsernameText {get; set;}
    private TMP_InputField PasswordText {get; set;}
    
    void Start()
    {
        authManager.GetComponent<AuthManager>().LoginToken();

        UsernameText = GameObject.Find("Username").GetComponent<TMP_InputField>();
        PasswordText = GameObject.Find("Password").GetComponent<TMP_InputField>();
        UsernameText.ActivateInputField();
    }

    void Update()
    {
        bool isShiftDown = Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);
        if (UsernameText.isFocused && Input.GetKeyDown(KeyCode.Tab) && !isShiftDown)
        {
            PasswordText.ActivateInputField();
        }

        if (PasswordText.isFocused && Input.GetKeyDown(KeyCode.Tab) && isShiftDown)
        {
            UsernameText.ActivateInputField();
        }

        if (Input.GetKeyDown(KeyCode.Return))
        {
            authManager.GetComponent<AuthManager>().Login();
        }
    }

    public void GoRegister()
    {
        SceneManager.LoadScene("Register");
    }
}
