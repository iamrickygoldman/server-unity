﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Friends : MonoBehaviour
{
    public GameObject friendsManagerObject;

    private FriendsManager FriendsManager {get; set;}

    void Awake()
    {
        FriendsManager.ReloadFriends = true;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            FriendsManager.RequestFriend();
        }
    }
}
