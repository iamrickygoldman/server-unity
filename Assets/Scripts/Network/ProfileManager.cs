﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ProfileManager : MonoBehaviour
{
    public static int ProfileId {get; set;} = 0;
    public static Profile Profile {get; set;}
    public static bool ReloadProfile {get; set;} = false;
    public static bool UpdateProfile {get; set;} = false;

    public GameObject friendsManagerObject;
    public GameObject enemiesManagerObject;
    public FriendsManager FriendsManager {get; set;}
    public EnemiesManager EnemiesManager {get; set;}

    private Button RequestFriend {get; set;}
    private Button AcceptFriend {get; set;}
    private Button RejectFriend {get; set;}
    private Button RemoveFriend {get; set;}
    private Button Block {get; set;}
    private Button Unblock {get; set;}

    void Start()
    {
        FriendsManager = friendsManagerObject.GetComponent<FriendsManager>();
        EnemiesManager = enemiesManagerObject.GetComponent<EnemiesManager>();

        RequestFriend = GameObject.Find("Request Friend").GetComponent<Button>();
        AcceptFriend = GameObject.Find("Accept Friend").GetComponent<Button>();
        RejectFriend = GameObject.Find("Reject Friend").GetComponent<Button>();
        RemoveFriend = GameObject.Find("Remove Friend").GetComponent<Button>();
        Block = GameObject.Find("Block").GetComponent<Button>();
        Unblock = GameObject.Find("Unblock").GetComponent<Button>();

        RequestFriend.gameObject.SetActive(false);
        AcceptFriend.gameObject.SetActive(false);
        RejectFriend.gameObject.SetActive(false);
        RemoveFriend.gameObject.SetActive(false);
        Block.gameObject.SetActive(false);
        Unblock.gameObject.SetActive(false);
    }

    void Update()
    {
        if (ReloadProfile)
        {
            StartCoroutine(ProfileHelper.GetProfile(AuthHelper.GetId(), AuthHelper.GetToken(), ProfileId));
            ReloadProfile = false;
        }

        if (UpdateProfile && !(Object.ReferenceEquals(null, Profile)))
        {
            TextMeshProUGUI usernameText = GameObject.Find("Username").GetComponent<TextMeshProUGUI>();
            usernameText.text = Profile.Username;
            TextMeshProUGUI mmrText = GameObject.Find("MMR").GetComponent<TextMeshProUGUI>();
            mmrText.text = Profile.Mmr.ToString();

            RequestFriend.onClick.RemoveAllListeners();
            AcceptFriend.onClick.RemoveAllListeners();
            RejectFriend.onClick.RemoveAllListeners();
            RemoveFriend.onClick.RemoveAllListeners();
            Block.onClick.RemoveAllListeners();
            Unblock.onClick.RemoveAllListeners();

            Debug.Log("Profile Can Accept: " + Profile.CanAcceptFriend);
            if (Profile.CanAcceptFriend)
            {
                RequestFriend.gameObject.SetActive(false);
                AcceptFriend.gameObject.SetActive(true);
                RejectFriend.gameObject.SetActive(true);
                RemoveFriend.gameObject.SetActive(false);
                Block.gameObject.SetActive(false);
                Unblock.gameObject.SetActive(false);

                AcceptFriend.onClick.AddListener(delegate { 
                    FriendsManager.AcceptFriend(Profile.Username);
                });
                RejectFriend.onClick.AddListener(delegate { 
                    FriendsManager.RejectFriend(Profile.Username);
                });
            }
            else if (Profile.IsFriend)
            {
                RequestFriend.gameObject.SetActive(false);
                AcceptFriend.gameObject.SetActive(false);
                RejectFriend.gameObject.SetActive(false);
                RemoveFriend.gameObject.SetActive(true);
                Block.gameObject.SetActive(false);
                Unblock.gameObject.SetActive(false);

                RemoveFriend.onClick.AddListener(delegate { 
                    FriendsManager.SoftRemoveFriend(Profile.Username);
                });
            }
            else if (Profile.IsBlocked)
            {
                RequestFriend.gameObject.SetActive(false);
                AcceptFriend.gameObject.SetActive(false);
                RejectFriend.gameObject.SetActive(false);
                RemoveFriend.gameObject.SetActive(false);
                Block.gameObject.SetActive(false);
                Unblock.gameObject.SetActive(true);

                Unblock.onClick.AddListener(delegate { 
                    EnemiesManager.Unblock(Profile.Username);
                });
            }
            else
            {
                RequestFriend.gameObject.SetActive(true);
                AcceptFriend.gameObject.SetActive(false);
                RejectFriend.gameObject.SetActive(false);
                RemoveFriend.gameObject.SetActive(false);
                Block.gameObject.SetActive(true);
                Unblock.gameObject.SetActive(false);

                RequestFriend.onClick.AddListener(delegate { 
                    FriendsManager.RequestFriend(Profile.Username);
                });
                Block.onClick.AddListener(delegate { 
                    EnemiesManager.Block(Profile.Username);
                });
            }

            UpdateProfile = false;
        }
    }
}
