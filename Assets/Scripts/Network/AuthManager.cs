﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class AuthManager : MonoBehaviour
{
    public static bool IsLoggingIn {get; set;} = false;
    public static bool TokenValid {get; set;} = false;

    void Update()
    {
        if (IsLoggingIn)
        {
            if (TokenValid)
            {
                Debug.Log("valid token!");
            }
        }
    }

    public void Register()
    {
        TMP_InputField usernameText = GameObject.Find("Username").GetComponent<TMP_InputField>();
        TMP_InputField emailText = GameObject.Find("Email").GetComponent<TMP_InputField>();
        TMP_InputField passwordText = GameObject.Find("Password").GetComponent<TMP_InputField>();
        TMP_InputField confirmPasswordText = GameObject.Find("Confirm Password").GetComponent<TMP_InputField>();
        TextMeshProUGUI errorText = GameObject.Find("Invalid").GetComponent<TextMeshProUGUI>();
        Debug.Log(usernameText.text + " : " + emailText.text + " : " + passwordText.text);
        if (usernameText.text == "")
        {
            errorText.text = "Enter a username.";
        }
        else if (emailText.text == "")
        {
            errorText.text = "Enter an email.";
        }
        else if (passwordText.text.Length < 6)
        {
            errorText.text = "Enter a password over 6 characters.";
        }
        else if (passwordText.text != confirmPasswordText.text)
        {
            errorText.text = "Passwords do not match.";
        }
        else
        {
            StartCoroutine(AuthHelper.Register(usernameText.text,emailText.text,passwordText.text, confirmPasswordText.text));
        }
    }

    public void Login()
    {
        TMP_InputField usernameText = GameObject.Find("Username").GetComponent<TMP_InputField>();
        TMP_InputField passwordText = GameObject.Find("Password").GetComponent<TMP_InputField>();
        TextMeshProUGUI errorText = GameObject.Find("Invalid").GetComponent<TextMeshProUGUI>();
        Debug.Log(usernameText.text + " : " + passwordText.text);
        if (usernameText.text == "")
        {
            errorText.text = "Enter a username";
        }
        else if (passwordText.text == "")
        {
            errorText.text = "Enter a password";
        }
        else
        {
            StartCoroutine(AuthHelper.Login(usernameText.text,passwordText.text));
        }
    }

    public void LoginToken()
    {
        Debug.Log(AuthHelper.GetId() + ": " + AuthHelper.GetToken());
        StartCoroutine(AuthHelper.Login(AuthHelper.GetId(), AuthHelper.GetToken()));
        IsLoggingIn = true;
    }
}
