﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class EnemiesManager : MonoBehaviour
{
    public GameObject enemyList;
    public GameObject enemyPrefab;
    public GameObject enemiesHeadingPrefab;

    public static Dictionary<int, string> Enemies {get; set;}
    public static Dictionary<int, string> Pending {get; set;}
    public static bool ReloadEnemies {get; set;} = false;
    public static bool UpdateEnemies {get; set;} = false;

    public void Start()
    {
        Enemies = new Dictionary<int, string>();
    }

    public void Update()
    {
        if (ReloadEnemies && enemyList != null)
        {
            StartCoroutine(EnemiesHelper.GetEnemiesList(AuthHelper.GetId(), AuthHelper.GetToken()));
            ReloadEnemies = false;
        }
        if (UpdateEnemies)
        {
            GameObject[] enemyItemObjects = GameObject.FindGameObjectsWithTag("Enemy Item");
            foreach (GameObject item in enemyItemObjects)
            {
                Destroy(item);
            }
            GameObject enemiesHeading = Instantiate(enemiesHeadingPrefab);
            enemiesHeading.transform.SetParent(enemyList.transform);
            enemiesHeading.transform.localScale = new Vector3(1, 1, 1);
            foreach (KeyValuePair<int, string> enemy in Enemies)
            {
                GameObject newEnemy = Instantiate(enemyPrefab);
                newEnemy.transform.SetParent(enemyList.transform);
                newEnemy.transform.localScale = new Vector3(1,1,1);
                Button viewButton = newEnemy.transform.Find("View").GetComponent<Button>();
                TextMeshProUGUI enemyName = viewButton.gameObject.transform.Find("View Text").gameObject.GetComponent<TextMeshProUGUI>();
                enemyName.text = enemy.Value;
                Button removeButton = newEnemy.transform.Find("Remove").GetComponent<Button>();
                removeButton.onClick.AddListener(delegate { Unblock(enemy.Value); });
            }
            UpdateEnemies = false;
        }
    }

    public void Block()
    {
        TMP_InputField blockText = GameObject.Find("Username Input").GetComponent<TMP_InputField>();
        if (blockText.text.Length > 0)
        {
            StartCoroutine(EnemiesHelper.Block(AuthHelper.GetId(), AuthHelper.GetToken(), blockText.text));
        }
    }

    public void Block(string enemy)
    {
        StartCoroutine(EnemiesHelper.Block(AuthHelper.GetId(), AuthHelper.GetToken(), enemy));
    }

    public void Unblock(string enemy)
    {
        StartCoroutine(EnemiesHelper.Unblock(AuthHelper.GetId(), AuthHelper.GetToken(), enemy));
    }
}
