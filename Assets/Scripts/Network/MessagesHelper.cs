﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Boomlagoon.JSON;
using TMPro;

public class MessagesHelper
{
    public static IEnumerator GetMessagesList(int id, string token, int otherId)
    {
        var formData = new WWWForm();
        formData.AddField("id", id);
        formData.AddField("token", token);
        formData.AddField("other_id", otherId);

        UnityWebRequest www = UnityWebRequest.Post(Network.BASE_URL + "/api/user/messages/list", formData);
        yield return www.SendWebRequest();
 
        if(www.isNetworkError || www.isHttpError) {
            Debug.Log(www.error);
        }
        else {
            Debug.Log(www.downloadHandler.text);
            JSONObject parsedObject = JSONObject.Parse(www.downloadHandler.text);
            bool success = parsedObject.GetBoolean("success");
            if (success)
            {
                MessagesManager.Messages = new List<Message>();
                JSONArray messages = parsedObject.GetArray("messages");
                foreach (JSONValue val in messages)
                {
                    JSONObject obj = val.Obj;
                    string tmpMessage = obj.GetString("message");
                    string tmpTimeSent = obj.GetString("time_sent");
                    bool tmpReceived = obj.GetBoolean("received");
                    int tmpSenderId = (int)obj.GetNumber("sender_id");
                    Message message = new Message(tmpMessage, tmpTimeSent, tmpReceived, tmpSenderId);
                    MessagesManager.Messages.Add(message);
                }
                MessagesManager.UpdateChat = true;
            }
            else
            {
                int error = (int)parsedObject.GetNumber("error");
                string message = parsedObject.GetString("message");
                Debug.Log(error + ": " + message);
            }
        }
    }

    public static IEnumerator SendMessage(int id, string token, int otherId, string content)
    {
        var formData = new WWWForm();
        formData.AddField("id", id);
        formData.AddField("token", token);
        formData.AddField("receiver_id", otherId);
        formData.AddField("message", content);

        UnityWebRequest www = UnityWebRequest.Post(Network.BASE_URL + "/api/user/message/send", formData);
        yield return www.SendWebRequest();
 
        if(www.isNetworkError || www.isHttpError) {
            Debug.Log(www.error);
        }
        else {
            Debug.Log(www.downloadHandler.text);
            JSONObject parsedObject = JSONObject.Parse(www.downloadHandler.text);
            if (parsedObject == null)
            {
                Debug.Log("Invalid Response");
                MessagesManager.UpdateChat = false;
            }
            else
            {
                bool success = parsedObject.GetBoolean("success");
                if (success)
                {
                    MessagesManager.ReloadChat = otherId;
                    Network.Instance.Send("%m" + Network.PackString(otherId, "2"));
                }
                else
                {
                    int error = (int)parsedObject.GetNumber("error");
                    string message = parsedObject.GetString("message");
                    Debug.Log(error + ": " + message);
                }
            }
        }
    }
}

public struct Message
{
    public string Content {get; set;}
    public string TimeSent {get; set;}
    public bool Received {get; set;}
    public int SenderId {get; set;}

    public Message(string content, string timeSent, bool received, int senderId)
    {
        this.Content = content;
        this.TimeSent = timeSent;
        this.Received = received;
        this.SenderId = senderId;
    }
}