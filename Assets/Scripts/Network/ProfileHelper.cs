﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using Boomlagoon.JSON;
using TMPro;

public class ProfileHelper : MonoBehaviour
{
    public static IEnumerator GetProfile(int id, string token, int userId)
    {
        var formData = new WWWForm();
        formData.AddField("id", id);
        formData.AddField("token", token);
        formData.AddField("user_id", userId);

        UnityWebRequest www = UnityWebRequest.Post(Network.BASE_URL + "/api/user/profile", formData);
        yield return www.SendWebRequest();
 
        if(www.isNetworkError || www.isHttpError) {
            Debug.Log(www.error);
        }
        else {
            Debug.Log(www.downloadHandler.text);
            JSONObject parsedObject = JSONObject.Parse(www.downloadHandler.text);
            bool success = parsedObject.GetBoolean("success");
            if (success)
            {
                JSONObject obj = parsedObject.GetObject("profile");
                int uid = (int)obj.GetNumber("id");
                string username = obj.GetString("username");
                int mmr = (int)obj.GetNumber("mmr");
                bool isPending = obj.GetBoolean("is_pending");
                bool canAcceptFriend = obj.GetBoolean("can_accept_friend");
                bool isFriend = obj.GetBoolean("is_friend");
                bool isBlocked = obj.GetBoolean("is_blocked");
                ProfileManager.Profile = new Profile(uid, username, mmr, isPending, canAcceptFriend, isFriend, isBlocked);
                ProfileManager.UpdateProfile = true;
            }
            else
            {
                int error = (int)parsedObject.GetNumber("error");
                string message = parsedObject.GetString("message");
                Debug.Log(error + ": " + message);
                SceneManager.LoadScene("Login");
            }
        }
    }
}

public struct Profile
{
    public int Id {get; set;}
    public string Username {get; set;}
    public int Mmr {get; set;}
    public bool IsPending {get; set;}
    public bool CanAcceptFriend {get; set;}
    public bool IsFriend {get; set;}
    public bool IsBlocked {get; set;}

    public Profile(int id, string username, int mmr, bool isPending, bool canAcceptFriend, bool isFriend, bool isBlocked)
    {
        this.Id = id;
        this.Username = username;
        this.Mmr = mmr;
        this.IsPending = isPending;
        this.CanAcceptFriend = canAcceptFriend;
        this.IsFriend = isFriend;
        this.IsBlocked = isBlocked;
    }
}