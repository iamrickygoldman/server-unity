﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class Matchmaking : MonoBehaviour
{
    public static List<string> QueuedHosts {get; set;}
    public static bool QueueUpdateHosts {get; set;} = false;
    public static bool QueueJoin {get; set;} = false;
    public static bool Catchup {get; set;} = true;

    public GameObject waitingList;
    public GameObject waitingPlayerPrefab;
    public GameObject matchmakingOverlay;
    public GameObject hostingOverlay;

    void Awake()
    {
        Catchup = true;
    }
    
    void Update()
    {
        if (Catchup && Network.Instance != null)
        {
            QueuedHosts = new List<string>();
            Network.Instance.Send("?h");
            Catchup = false;
            hostingOverlay.GetComponent<Popup>().Hide();
            matchmakingOverlay.GetComponent<Popup>().Hide();
        }

        if (QueueUpdateHosts)
        {
            QueueUpdateHosts = false;
            ClearWaitingPlayers();
            for (int i = QueuedHosts.Count - 1; i >= 0; i--)
            {
                string text = QueuedHosts[i];
                string username = Network.ExtractString(text, "A");
                if (username == "") { return; }
                int userId = Network.ExtractInt(text, "1");
                if (userId == Network.NULL_INT) { return; }
                int mmr = Network.ExtractInt(text, "a");
                if (mmr == Network.NULL_INT) { return; }
                int matchId = Network.ExtractInt(text, "M");
                if (matchId == Network.NULL_INT) { return; }
                AddWaitingPlayer(matchId, userId, username, mmr);
            }
        }

        if (QueueJoin)
        {
            QueueJoin = false;
            SceneManager.LoadScene("Test");
        }
    }

    public void HostMatch()
    {
        Network.Instance.Send("#h");
        hostingOverlay.GetComponent<Popup>().Display();
    }

    public void CancelHost()
    {
        Network.Instance.Send("#H");
        hostingOverlay.GetComponent<Popup>().Hide();
    }

    public void JoinMatch(int matchId)
    {
        Network.Instance.Send("#j" + Network.PackString(matchId, "M"));
    }

    public void AddWaitingPlayer(int matchId, int userId, string username, int mmr)
    {
        Debug.Log("adding " + username);
        var player = Instantiate(waitingPlayerPrefab, new Vector3(0, 0, 0), Quaternion.identity);
        player.transform.localScale = Vector3.one;
        player.transform.SetParent(waitingList.transform, false);
        Button joinButton = player.transform.Find("Join").gameObject.GetComponent<Button>();
        TextMeshProUGUI joinText = joinButton.transform.Find("Text").gameObject.GetComponent<TextMeshProUGUI>();
        joinText.SetText(username);
        TextMeshProUGUI mmrText = joinButton.transform.Find("MMR Text").gameObject.GetComponent<TextMeshProUGUI>();
        mmrText.SetText(mmr.ToString());
        joinButton.onClick.AddListener(delegate { JoinMatch(matchId); });
    }

    public void ClearWaitingPlayers()
    {
        GameObject[] items = GameObject.FindGameObjectsWithTag("Host Player");
        foreach(GameObject item in items)
        {
            Destroy(item);
        }
    }

    public void JoinMatchmaking()
    {
        Network.Instance.Send("#m");
        matchmakingOverlay.GetComponent<Popup>().Display();
    }

    public void CancelMatchmaking()
    {
        Network.Instance.Send("#M");
        matchmakingOverlay.GetComponent<Popup>().Hide();
    }
}