﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class FriendsManager : MonoBehaviour
{
    public GameObject friendList;
    public GameObject friendPrefab;
    public GameObject pendingPrefab;
    public GameObject friendsHeadingPrefab;
    public GameObject pendingHeadingPrefab;

    public static List<Friend> Friends {get; set;}
    public static List<Friend> Pending {get; set;}
    public static bool ReloadFriends {get; set;} = false;
    public static bool UpdateFriends {get; set;} = false;

    public void Start()
    {
        Friends = new List<Friend>();
        Pending = new List<Friend>();
    }

    public void Update()
    {
        if (ReloadFriends && friendList != null)
        {
            StartCoroutine(FriendsHelper.GetFriendsList(AuthHelper.GetId(), AuthHelper.GetToken()));
            ReloadFriends = false;
        }
        if (UpdateFriends)
        {
            GameObject[] friendItemObjects = GameObject.FindGameObjectsWithTag("Friend Item");
            foreach (GameObject item in friendItemObjects)
            {
                Destroy(item);
            }
            if (Pending.Count > 0)
            {
                GameObject pendingHeading = Instantiate(pendingHeadingPrefab);
                pendingHeading.transform.SetParent(friendList.transform);
                pendingHeading.transform.localScale = new Vector3(1, 1, 1);
            }
            foreach (Friend friend in Pending)
            {
                GameObject newFriend = Instantiate(pendingPrefab);
                newFriend.transform.SetParent(friendList.transform);
                newFriend.transform.localScale = new Vector3(1, 1, 1);
                Button viewButton = newFriend.transform.Find("View").GetComponent<Button>();
                viewButton.onClick.AddListener(delegate { ViewPlayer(friend.Id); });
                TextMeshProUGUI friendName = viewButton.gameObject.transform.Find("View Text").gameObject.GetComponent<TextMeshProUGUI>();
                friendName.text = friend.Username;
                TextMeshProUGUI friendMmr = viewButton.gameObject.transform.Find("MMR Text").gameObject.GetComponent<TextMeshProUGUI>();
                friendMmr.text = friend.Mmr.ToString();
                Button acceptButton = newFriend.transform.Find("Accept").GetComponent<Button>();
                acceptButton.onClick.AddListener(delegate { AcceptFriend(friend.Username); });
                Button rejectButton = newFriend.transform.Find("Reject").GetComponent<Button>();
                rejectButton.onClick.AddListener(delegate { RejectFriend(friend.Username); });
            }
            GameObject friendsHeading = Instantiate(friendsHeadingPrefab);
            friendsHeading.transform.SetParent(friendList.transform);
            friendsHeading.transform.localScale = new Vector3(1, 1, 1);
            foreach (Friend friend in Friends)
            {
                GameObject newFriend = Instantiate(friendPrefab);
                newFriend.transform.SetParent(friendList.transform);
                newFriend.transform.localScale = new Vector3(1,1,1);
                Button viewButton = newFriend.transform.Find("View").GetComponent<Button>();
                viewButton.onClick.AddListener(delegate { ViewPlayer(friend.Id); });
                TextMeshProUGUI friendName = viewButton.gameObject.transform.Find("View Text").gameObject.GetComponent<TextMeshProUGUI>();
                friendName.text = friend.Username;
                TextMeshProUGUI friendMmr = viewButton.gameObject.transform.Find("MMR Text").gameObject.GetComponent<TextMeshProUGUI>();
                friendMmr.text = friend.Mmr.ToString();
                Button messageButton = newFriend.transform.Find("Message").GetComponent<Button>();
                messageButton.onClick.AddListener(delegate { MessageFriend(friend.Id, friend.Username); });
                Button removeButton = newFriend.transform.Find("Remove").GetComponent<Button>();
                removeButton.onClick.AddListener(delegate { SoftRemoveFriend(friend.Username); });
            }
            UpdateFriends = false;
        }
    }

    public void RequestFriend()
    {
        TMP_InputField addFriendText = GameObject.Find("Add Friend").GetComponent<TMP_InputField>();
        if (addFriendText.text.Length > 0)
        {
            StartCoroutine(FriendsHelper.RequestFriend(AuthHelper.GetId(), AuthHelper.GetToken(), addFriendText.text));
            addFriendText.text = "";
        }
    }

    public void RequestFriend(string friend)
    {
        StartCoroutine(FriendsHelper.RequestFriend(AuthHelper.GetId(), AuthHelper.GetToken(), friend));
    }

    public void AcceptFriend(string friend)
    {
        StartCoroutine(FriendsHelper.AcceptFriend(AuthHelper.GetId(), AuthHelper.GetToken(), friend));
    }

    public void RejectFriend(string friend)
    {
        StartCoroutine(FriendsHelper.RejectFriend(AuthHelper.GetId(), AuthHelper.GetToken(), friend));
    }

    public void SoftRemoveFriend(string friend)
    {
        GameObject removeFriendConfirmation = GameObject.Find("Remove Friend Confirmation");
        Transform removeFriendContent = removeFriendConfirmation.transform.Find("Content");
        TextMeshProUGUI usernameText = removeFriendContent.Find("Username Text").gameObject.GetComponent<TextMeshProUGUI>();
        usernameText.text = usernameText.text.Replace("{username}", friend);
        Button confirmButton = removeFriendContent.Find("Confirm").GetComponent<Button>();
        Button cancelButton = removeFriendContent.Find("Cancel").GetComponent<Button>();
        confirmButton.onClick.AddListener(delegate {
            RemoveFriend(friend);
            removeFriendConfirmation.GetComponent<Popup>().Hide();
            cancelButton.onClick.RemoveAllListeners();
            confirmButton.onClick.RemoveAllListeners();
        });
        cancelButton.onClick.AddListener(delegate { 
            usernameText.text = usernameText.text.Replace(friend, "{username}");
            removeFriendConfirmation.GetComponent<Popup>().Hide();
            cancelButton.onClick.RemoveAllListeners();
            confirmButton.onClick.RemoveAllListeners();
        });
        removeFriendConfirmation.GetComponent<Popup>().Display();
    }

    public void RemoveFriend(string friend)
    {
        StartCoroutine(FriendsHelper.RemoveFriend(AuthHelper.GetId(), AuthHelper.GetToken(), friend));
    }

    public void ViewPlayer(int id)
    {
        ProfileManager.ProfileId = id;
        SceneManager.LoadScene("Profile View");
    }

    public void MessageFriend(int id, string friend)
    {
        MessagesManager.OpenChat(id, friend);
    }
}
