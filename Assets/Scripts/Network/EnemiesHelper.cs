﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using Boomlagoon.JSON;
using TMPro;

public class EnemiesHelper : MonoBehaviour
{
    public static IEnumerator GetEnemiesList(int id, string token)
    {
        var formData = new WWWForm();
        formData.AddField("id", id);
        formData.AddField("token", token);

        UnityWebRequest www = UnityWebRequest.Post(Network.BASE_URL + "/api/user/enemies/list", formData);
        yield return www.SendWebRequest();
 
        if(www.isNetworkError || www.isHttpError) {
            Debug.Log(www.error);
        }
        else {
            Debug.Log(www.downloadHandler.text);
            JSONObject parsedObject = JSONObject.Parse(www.downloadHandler.text);
            bool success = parsedObject.GetBoolean("success");
            if (success)
            {
                EnemiesManager.Enemies = new Dictionary<int, string>();
                JSONArray enemies = parsedObject.GetArray("enemies");
                foreach (JSONValue val in enemies)
                {
                    JSONObject obj = val.Obj;
                    int tmp_id = (int)obj.GetNumber("id");
                    string tmp_username = obj.GetString("username");
                    EnemiesManager.Enemies.Add(tmp_id, tmp_username);
                }
                EnemiesManager.UpdateEnemies = true;
            }
            else
            {
                int error = (int)parsedObject.GetNumber("error");
                string message = parsedObject.GetString("message");
                Debug.Log(error + ": " + message);
                SceneManager.LoadScene("Login");
            }
        }
    }

    public static IEnumerator Block(int id, string token, string username)
    {
        var formData = new WWWForm();
        formData.AddField("id", id);
        formData.AddField("token", token);
        formData.AddField("username", username);

        UnityWebRequest www = UnityWebRequest.Post(Network.BASE_URL + "/api/user/block", formData);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log(www.downloadHandler.text);
            JSONObject parsedObject = JSONObject.Parse(www.downloadHandler.text);
            bool success = parsedObject.GetBoolean("success");
            if (success)
            {
                int enemyId = (int)parsedObject.GetNumber("enemy_id");
                Debug.Log("Blocked " + username);
                EnemiesManager.ReloadEnemies = true;
                ProfileManager.ReloadProfile = true;
                Network.Instance.Send("%e" + Network.PackString(enemyId, "2"));
            }
            else
            {
                int error = (int)parsedObject.GetNumber("error");
                string message = parsedObject.GetString("message");
                Debug.Log(error + ": " + message);
            }
        }
    }

    public static IEnumerator Unblock(int id, string token, string username)
    {
        var formData = new WWWForm();
        formData.AddField("id", id);
        formData.AddField("token", token);
        formData.AddField("username", username);

        UnityWebRequest www = UnityWebRequest.Post(Network.BASE_URL + "/api/user/unblock", formData);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log(www.downloadHandler.text);
            JSONObject parsedObject = JSONObject.Parse(www.downloadHandler.text);
            bool success = parsedObject.GetBoolean("success");
            if (success)
            {
                int enemyId = (int)parsedObject.GetNumber("enemy_id");
                Debug.Log("Unblocked " + username);
                EnemiesManager.ReloadEnemies = true;
                ProfileManager.ReloadProfile = true;
                Network.Instance.Send("%E" + Network.PackString(enemyId, "2"));
            }
            else
            {
                int error = (int)parsedObject.GetNumber("error");
                string message = parsedObject.GetString("message");
                Debug.Log(error + ": " + message);
            }
        }
    }
}
