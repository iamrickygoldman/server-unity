﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using Boomlagoon.JSON;
using TMPro;

public class FriendsHelper
{
    public static IEnumerator GetFriendsList(int id, string token)
    {
        var formData = new WWWForm();
        formData.AddField("id", id);
        formData.AddField("token", token);

        UnityWebRequest www = UnityWebRequest.Post(Network.BASE_URL + "/api/user/friends/list", formData);
        yield return www.SendWebRequest();
 
        if(www.isNetworkError || www.isHttpError) {
            Debug.Log(www.error);
        }
        else {
            Debug.Log(www.downloadHandler.text);
            JSONObject parsedObject = JSONObject.Parse(www.downloadHandler.text);
            bool success = parsedObject.GetBoolean("success");
            if (success)
            {
                FriendsManager.Friends = new List<Friend>();
                FriendsManager.Pending = new List<Friend>();
                JSONArray pending = parsedObject.GetArray("pending");
                foreach (JSONValue val in pending)
                {
                    JSONObject obj = val.Obj;
                    int tmp_id = (int)obj.GetNumber("id");
                    string tmp_username = obj.GetString("username");
                    int tmp_mmr = (int)obj.GetNumber("mmr");
                    Friend friend = new Friend(tmp_id, tmp_username, tmp_mmr);
                    FriendsManager.Pending.Add(friend);
                }
                JSONArray friends = parsedObject.GetArray("friends");
                foreach (JSONValue val in friends)
                {
                    JSONObject obj = val.Obj;
                    int tmp_id = (int)obj.GetNumber("id");
                    string tmp_username = obj.GetString("username");
                    int tmp_mmr = (int)obj.GetNumber("mmr");
                    Friend friend = new Friend(tmp_id, tmp_username, tmp_mmr);
                    FriendsManager.Friends.Add(friend);
                }
                FriendsManager.UpdateFriends = true;
            }
            else
            {
                int error = (int)parsedObject.GetNumber("error");
                string message = parsedObject.GetString("message");
                Debug.Log(error + ": " + message);
                SceneManager.LoadScene("Login");
            }
        }
    }

    public static IEnumerator RequestFriend(int id, string token, string username)
    {
        var formData = new WWWForm();
        formData.AddField("id", id);
        formData.AddField("token", token);
        formData.AddField("username", username);

        UnityWebRequest www = UnityWebRequest.Post(Network.BASE_URL + "/api/user/friend/request", formData);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log(www.downloadHandler.text);
            JSONObject parsedObject = JSONObject.Parse(www.downloadHandler.text);
            bool success = parsedObject.GetBoolean("success");
            if (success)
            {
                int friendId = (int)parsedObject.GetNumber("friend_id");
                Debug.Log("Friend request sent to " + username);
                Network.Instance.Send("%f" + Network.PackString(friendId, "2"));
            }
            else
            {
                int error = (int)parsedObject.GetNumber("error");
                string message = parsedObject.GetString("message");
                Debug.Log(error + ": " + message);
            }
        }
    }

    public static IEnumerator AcceptFriend(int id, string token, string username)
    {
        var formData = new WWWForm();
        formData.AddField("id", id);
        formData.AddField("token", token);
        formData.AddField("username", username);

        UnityWebRequest www = UnityWebRequest.Post(Network.BASE_URL + "/api/user/friend/accept", formData);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log(www.downloadHandler.text);
            JSONObject parsedObject = JSONObject.Parse(www.downloadHandler.text);
            bool success = parsedObject.GetBoolean("success");
            if (success)
            {
                int friendId = (int)parsedObject.GetNumber("friend_id");
                Debug.Log(username + " accepted as friend.");
                FriendsManager.ReloadFriends = true;
                ProfileManager.ReloadProfile = true;
                Network.Instance.Send("%a" + Network.PackString(friendId, "2"));
            }
            else
            {
                int error = (int)parsedObject.GetNumber("error");
                string message = parsedObject.GetString("message");
                Debug.Log(error + ": " + message);
            }
        }
    }

    public static IEnumerator RejectFriend(int id, string token, string username)
    {
        var formData = new WWWForm();
        formData.AddField("id", id);
        formData.AddField("token", token);
        formData.AddField("username", username);

        UnityWebRequest www = UnityWebRequest.Post(Network.BASE_URL + "/api/user/friend/reject", formData);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log(www.downloadHandler.text);
            JSONObject parsedObject = JSONObject.Parse(www.downloadHandler.text);
            bool success = parsedObject.GetBoolean("success");
            if (success)
            {
                int friendId = (int)parsedObject.GetNumber("friend_id");
                Debug.Log(username + " rejected as friend.");
                FriendsManager.ReloadFriends = true;
                ProfileManager.ReloadProfile = true;
                Network.Instance.Send("%r" + Network.PackString(friendId, "2"));
            }
            else
            {
                int error = (int)parsedObject.GetNumber("error");
                string message = parsedObject.GetString("message");
                Debug.Log(error + ": " + message);
            }
        }
    }

    public static IEnumerator RemoveFriend(int id, string token, string username)
    {
        var formData = new WWWForm();
        formData.AddField("id", id);
        formData.AddField("token", token);
        formData.AddField("username", username);

        UnityWebRequest www = UnityWebRequest.Post(Network.BASE_URL + "/api/user/friend/remove", formData);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log(www.downloadHandler.text);
            JSONObject parsedObject = JSONObject.Parse(www.downloadHandler.text);
            bool success = parsedObject.GetBoolean("success");
            if (success)
            {
                int friendId = (int)parsedObject.GetNumber("friend_id");
                Debug.Log(username + " removed as friend.");
                FriendsManager.ReloadFriends = true;
                ProfileManager.ReloadProfile = true;
                Network.Instance.Send("%F" + Network.PackString(friendId, "2"));
            }
            else
            {
                int error = (int)parsedObject.GetNumber("error");
                string message = parsedObject.GetString("message");
                Debug.Log(error + ": " + message);
            }
        }
    }
}

public struct Friend
{
    public int Id {get; set;}
    public string Username {get; set;}
    public int Mmr {get; set;}

    public Friend(int id, string username, int mmr)
    {
        this.Id = id;
        this.Username = username;
        this.Mmr = mmr;
    }
}
