﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Network : MonoBehaviour
{
    public const string BASE_URL = "http://server-core.loc";
    public const string ADDRESS = "127.0.0.1";

    private const float PING_TIME = 5f;

    private const string END = "`";
    public const int NULL_INT = Int32.MinValue;

    public static Network Instance {get; set;}

    public static bool QueueLogout {get; set;} = false;
    public static bool QueueDisconnect {get; set;} = false;

    public Socket Socket {get; set;}
    public bool Connected {get; set;} = false;
    public bool WasDisconnected {get; set;} = false;
    public bool LoggedIn {get; set;} = false;

    private float TimeSinceLastPing {get; set;} = 0f;
    private float TimeSinceLastLife {get; set;} = 0f;
    private bool QueueDisconnectPopupHide {get; set;} = false;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
            Initialize();
        }
        else
        {
            if (!Instance.Connected)
            {
                Instance.Initialize();
            }
            Destroy(gameObject);
        }
    }

    void OnApplicationQuit()
    {
        Close();
    }

    void OnDestroy()
    {
    #if UNITY_EDITOR
        Close();
    #endif
    }

    public void Initialize()
    {
        if (Connected)
        {
            return;
        }
        IPAddress ipAddress = IPAddress.Parse(ADDRESS);
        int port = 6373;

        try {
            IPEndPoint server = new IPEndPoint(ipAddress, port);
            Socket = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

            try {
                Socket.Connect(server);
                Connected = true;
                Debug.Log("Socket connected to " + Socket.RemoteEndPoint.ToString());

                Thread listenThread = new Thread(() => 
                {
                    Thread.CurrentThread.IsBackground = true;
                    StartListening();
                });
                listenThread.Start();

            } catch (Exception e) {
                Debug.Log("Exception 1 : " + e.ToString());
                Close();
            }
        } catch (Exception e) {
            Debug.Log("Exception 2 : " + e.ToString());
        }
    }

    void Update()
    {
        TimeSinceLastPing += Time.deltaTime;
        if (TimeSinceLastPing >= PING_TIME)
        {
            Send("#");
            TimeSinceLastPing = 0f;
        }
        TimeSinceLastLife += Time.deltaTime;
        if (TimeSinceLastLife > PING_TIME * 2)
        {
            GameObject disconconnectObject = GameObject.Find("Disconnected Overlay");
            if (disconconnectObject != null)
            {
                Popup disconnectPopup = disconconnectObject.GetComponent<Popup>();
                disconnectPopup.Display();
            }
            TimeSinceLastLife = 0f;
            WasDisconnected = true;
            Reconnect();
        }

        if (QueueDisconnectPopupHide)
        {
            GameObject disconconnectObject = GameObject.Find("Disconnected Overlay");
            if (disconconnectObject != null)
            {
                QueueDisconnectPopupHide = true;
                Popup disconnectPopup = disconconnectObject.GetComponent<Popup>();
                disconnectPopup.Hide();
            }
            QueueDisconnectPopupHide = false;
            if (WasDisconnected)
            {
                WasDisconnected = false;
                SceneManager.LoadScene("Login");
            }
        }

        if (QueueLogout)
        {
            QueueLogout = false;
            Logout();
        }

        if (QueueDisconnect)
        {
            QueueDisconnect = false;
            Disconnect();
        }
    }

    public void Reconnect()
    {
        Debug.Log("Reconnecting...");
        Close();
        Initialize();
    }

    public void Close()
    {
        if (Connected)
        {
            try {
                Socket.Close();
                Socket = null;
                Connected = false;
                LoggedIn = false;
            } catch (Exception e) {
                Debug.Log("Unexpected exception : " + e.ToString());
            }
        }
    }

    public void StartListening()
    {
        byte[] bytes = new Byte[1024];
        string received = null;
        while (Connected)
        {
            received = null;
            while (true)
            {
                try {
                    int bytesRec = Socket.Receive(bytes);
                    received += Encoding.ASCII.GetString(bytes, 0, bytesRec);
                    if (received.IndexOf(END) > -1)
                    {
                        break;
                    }
                } catch (Exception e) {
                    Debug.Log("Exception : " + e.ToString());
                    Close();
                    return;
                }
            }
            Debug.Log("Received: " + received);
            int endIndex = received.IndexOf(END);
            do
            {
                string chunk = received.Substring(0, endIndex + 1);
                if (endIndex == received.Length) {
                    break;
                }
                received = received.Substring(endIndex + 1);
                endIndex = received.IndexOf(END);
                Process(chunk);
            } while (endIndex > -1 && endIndex < received.Length);
        }
    }

    public bool Send(string message)
    {
        if (!Connected)
        {
            return false;
        }
        message = message.Replace(END, "");
        try {
            byte[] msg = Encoding.ASCII.GetBytes(message + END);
            int bytesSend = Socket.Send(msg);
            Debug.Log("Bytes: " + bytesSend);
            Debug.Log("Sent: " + Encoding.ASCII.GetString(msg,0,bytesSend));
            return true;
        } catch (Exception e) {
            Debug.Log("Exception : " + e.ToString());
            return false;
        }
    }

    public bool Login()
    {
        if (!LoggedIn)
        {
            if (Send("#i" + PackString(AuthHelper.GetId(), "U") + PackString(AuthHelper.GetToken(), "T") + PackString(AuthHelper.GetUsername(), "N") + PackString(AuthHelper.GetMmr(), "R")))
            {
                LoggedIn = true;
                Send("?m");
                Matchmaking.Catchup = true;
                FriendsManager.ReloadFriends = true;
                EnemiesManager.ReloadEnemies = true;
                ProfileManager.ReloadProfile = true;
                return true;
            }
        }
        LoggedIn = false;
        return false;
    }

    public bool Logout()
    {
        if (LoggedIn)
        {
            PlayerPrefs.DeleteKey("id");
            PlayerPrefs.DeleteKey("token");
            PlayerPrefs.DeleteKey("username");
            PlayerPrefs.DeleteKey("mmr");
            LoggedIn = false;
            bool result = Send("#o");
            Close();
            SceneManager.LoadScene("Login");
            return result;
        }
        return false;
    }

    public void Disconnect()
    {
        bool result = Send("#o");
        Close();
        SceneManager.LoadScene("Disconnected");
    }

    private void Process(string text)
    {
        if (text == "#" + END)
        {
            TimeSinceLastLife = 0f;
            QueueDisconnectPopupHide = true;
            return;
        }
        if (text.Length < 3)
        {
            return;
        }
        text = text.Substring(0, text.Length - 1);
        char type = text[0];
        char action = text[1];
        switch (type)
        {
            case '#':
                switch (action)
                {
                    // Log Out
                    case 'o':
                        QueueLogout = true;
                        break;
                    case 'd':
                        QueueDisconnect = true;
                        break;
                    // Host Created
                    case 'h':
                        Matchmaking.QueuedHosts.Add(text);
                        Debug.Log("queue " + text);
                        break;
                    case 'j':
                    case 'm':
                        int matchId = ExtractInt(text, "M");
                        if (matchId == NULL_INT) { return; }
                        int player1Id = ExtractInt(text, "1");
                        if (player1Id == NULL_INT) { return; }
                        int player2Id = ExtractInt(text, "2");
                        if (player2Id == NULL_INT) { return; }
                        string player1Username = ExtractString(text, "A");
                        if (player1Username == "") { return; }
                        string player2Username = ExtractString(text, "B");
                        if (player2Username == "") { return; }
                        GameState.MatchId = matchId;
                        GameState.Player1Id = player1Id;
                        GameState.Player1Username = player1Username;
                        GameState.Player2Id = player2Id;
                        GameState.Player2Username = player2Username;
                        Matchmaking.QueueJoin = true;
                        break;
                    case 'q':
                    case 'a':
                        GameState.Clear();
                        TestGame.GameOver = true;
                        break;
                    case 'l':
                        TestGame.OpponentConnected = true;
                        break;
                }
                break;
            case '?':
                switch (action)
                {
                    // Host Query
                    case 'h':
                        Matchmaking.QueuedHosts = new List<string>();
                        Debug.Log("clear hosts");
                        break;
                    case 'H':
                        Matchmaking.QueueUpdateHosts = true;
                        break;
                }
                break;
            case '%':
                switch (action)
                {
                    // Friend Request
                    case 'f':
                        Debug.Log("Received f");
                        FriendsManager.ReloadFriends = true;
                        ProfileManager.ReloadProfile = true;
                        break;
                    // Friend Accept
                    case 'a':
                        FriendsManager.ReloadFriends = true;
                        ProfileManager.ReloadProfile = true;
                        break;
                    // Friend Reject
                    case 'r':
                        FriendsManager.ReloadFriends = true;
                        ProfileManager.ReloadProfile = true;
                        break;
                    // Friend Remove
                    case 'F':
                        FriendsManager.ReloadFriends = true;
                        ProfileManager.ReloadProfile = true;
                        break;
                    // Blocked User
                    case 'e':
                        EnemiesManager.ReloadEnemies = true;
                        ProfileManager.ReloadProfile = true;
                        break;
                    // Unblocked User
                    case 'E':
                        EnemiesManager.ReloadEnemies = true;
                        ProfileManager.ReloadProfile = true;
                        break;
                    // Message Received
                    case 'm':
                        int otherId = ExtractInt(text, "1");
                        MessagesManager.ReloadChat = otherId;
                        Debug.Log("message from " + otherId);
                        break;
                }
                break;
            case '!':
                switch (action)
                {
                    // Test Action
                    case 't':
                        TestBox.QueueChange = text;
                        break;
                }
                break;
        }
    }

    public static string ExtractString(string text, string marker)
    {
        int startIndex = text.IndexOf("{" + marker);
        if (startIndex < 0)
        {
            return "";
        }
        int stopIndex = text.IndexOf(marker + "}");
        if (stopIndex - startIndex - 2 < 1)
        {
            return "";
        }
        return text.Substring(startIndex + 2, stopIndex - startIndex - 2);
    }

    public static int ExtractInt(string text, string marker)
    {
        int startIndex = text.IndexOf("{" + marker);
        if (startIndex < 0)
        {
            return NULL_INT;
        }
        int stopIndex = text.IndexOf(marker + "}");
        if (stopIndex - startIndex - 2 < 1)
        {
            return NULL_INT;
        }
        string extractedString = text.Substring(startIndex + 2, stopIndex - startIndex - 2);
        int extractedInt;
        if (Int32.TryParse(extractedString, out extractedInt))
        {
            return extractedInt;
        }
        return NULL_INT;
    }

    public static string PackString(string data, string marker)
    {
        return "{" + marker + data + marker + "}";
    }

    public static string PackString(int data, string marker)
    {
        return "{" + marker + data + marker + "}";
    }
    
}
