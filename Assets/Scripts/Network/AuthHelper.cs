﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using Boomlagoon.JSON;
using TMPro;

public static class AuthHelper
{
    public readonly static string NULL_TOKEN = new string('x', 60);

    public static IEnumerator Register(string username, string email, string password, string confirmPassword)
    {
        var formData = new WWWForm();
        formData.AddField("username", username);
        formData.AddField("email", email);
        formData.AddField("password", password);
        formData.AddField("password_confirmation", confirmPassword);

        UnityWebRequest www = UnityWebRequest.Post(Network.BASE_URL + "/api/user/register", formData);
        yield return www.SendWebRequest();

        TextMeshProUGUI errorText = GameObject.Find("Invalid").GetComponent<TextMeshProUGUI>();
 
        if(www.isNetworkError || www.isHttpError) {
            Debug.Log(www.error);
            errorText.text = "Could not connect to network.";
            Network.Instance.LoggedIn = false;
        }
        else {
            JSONObject parsedObject = JSONObject.Parse(www.downloadHandler.text);
            bool success = parsedObject.GetBoolean("success");
            if (success)
            {
                int id = (int)parsedObject.GetNumber("id");
                string token = parsedObject.GetString("token");
                int mmr = (int)parsedObject.GetNumber("mmr");
                PlayerPrefs.SetInt("id", id);
                PlayerPrefs.SetString("token", token);
                PlayerPrefs.SetString("username", username);
                PlayerPrefs.SetInt("mmr", mmr);
                PlayerPrefs.Save();
                Network.Instance.Login();
                SceneManager.LoadScene("Lobby");
            }
            else
            {
                int error = (int)parsedObject.GetNumber("error");
                string message = parsedObject.GetString("message");
                Debug.Log(error + ": " + message);
                PlayerPrefs.DeleteKey("id");
                PlayerPrefs.DeleteKey("token");
                PlayerPrefs.DeleteKey("username");
                PlayerPrefs.DeleteKey("mmr");
                errorText.text = message;
                Network.Instance.Logout();
            }
        }
    }
    
    public static IEnumerator Login(string username, string password)
    {
        var formData = new WWWForm();
        formData.AddField("username", username);
        formData.AddField("password", password);

        UnityWebRequest www = UnityWebRequest.Post(Network.BASE_URL + "/api/user/login", formData);
        yield return www.SendWebRequest();

        TextMeshProUGUI errorText = GameObject.Find("Invalid").GetComponent<TextMeshProUGUI>();
 
        if(www.isNetworkError || www.isHttpError) {
            Debug.Log(www.error);
            errorText.text = "Could not connect to network.";
            Network.Instance.LoggedIn = false;
        }
        else {
            JSONObject parsedObject = JSONObject.Parse(www.downloadHandler.text);
            bool success = parsedObject.GetBoolean("success");
            if (success)
            {
                int id = (int)parsedObject.GetNumber("id");
                string token = parsedObject.GetString("token");
                int mmr = (int)parsedObject.GetNumber("mmr");
                PlayerPrefs.SetInt("id", id);
                PlayerPrefs.SetString("token", token);
                PlayerPrefs.SetString("username", username);
                PlayerPrefs.SetInt("mmr", mmr);
                PlayerPrefs.Save();
                Network.Instance.Login();
                SceneManager.LoadScene("Lobby");
            }
            else
            {
                int error = (int)parsedObject.GetNumber("error");
                string message = parsedObject.GetString("message");
                Debug.Log(error + ": " + message);
                PlayerPrefs.DeleteKey("id");
                PlayerPrefs.DeleteKey("token");
                PlayerPrefs.DeleteKey("username");
                PlayerPrefs.DeleteKey("mmr");
                errorText.text = message;
                Network.Instance.Logout();
            }
        }
    }

    public static IEnumerator Login(int id, string token)
    {
        var formData = new WWWForm();
        formData.AddField("id", id);
        formData.AddField("token", token);

        UnityWebRequest www = UnityWebRequest.Post(Network.BASE_URL + "/api/user/login/token", formData);
        yield return www.SendWebRequest();
 
        if(www.isNetworkError || www.isHttpError) {
            Debug.Log(www.error);
            Network.Instance.LoggedIn = false;
        }
        else {
            Debug.Log(www.downloadHandler.text);
            JSONObject parsedObject = JSONObject.Parse(www.downloadHandler.text);
            bool success = parsedObject.GetBoolean("success");
            if (success)
            {
                string username = parsedObject.GetString("username");
                int mmr = (int)parsedObject.GetNumber("mmr");
                PlayerPrefs.SetString("username", username);
                PlayerPrefs.SetInt("mmr", mmr);
                PlayerPrefs.Save();
                Network.Instance.Login();
                SceneManager.LoadScene("Lobby");
            }
            else
            {
                int error = (int)parsedObject.GetNumber("error");
                string message = parsedObject.GetString("message");
                Debug.Log(error + ": " + message);
                PlayerPrefs.DeleteKey("id");
                PlayerPrefs.DeleteKey("token");
                PlayerPrefs.DeleteKey("username");
                PlayerPrefs.DeleteKey("mmr");
                Network.Instance.Logout();
            }
        }
    }

    public static bool IsLoggedIn()
    {
        string nullToken = NULL_TOKEN;
        return GetId() != -1 && GetToken() != nullToken;
    }

    public static int GetId()
    {
        return PlayerPrefs.GetInt("id", -1);
    }

    public static string GetToken()
    {
        return PlayerPrefs.GetString("token", NULL_TOKEN);
    }

    public static string GetUsername()
    {
        return PlayerPrefs.GetString("username", "");
    }

    public static int GetMmr()
    {
        return PlayerPrefs.GetInt("mmr", Network.NULL_INT);
    }
}