﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MessagesManager : MonoBehaviour
{
    public GameObject chatPrefab;
    private static GameObject ChatPrefabStatic {get; set;}
    public GameObject myMessagePrefab;
    public GameObject theirMessagePrefab;

    public static List<Message> Messages {get; set;}
    public static int ReloadChat {get; set;} = -1;
    public static bool UpdateChat {get; set;} = false;
    public static int OtherId {get; set;} = -1;
    public static string SendText {get; set;} = "";

    private static TMP_InputField MessageText {get; set;}
    private static int CurrentId {get; set;}

    void Start()
    {
        Messages = new List<Message>();
        ChatPrefabStatic = chatPrefab;
    }

    void Update()
    {
        if (MessageText != null && Input.GetKeyDown(KeyCode.Return))
        {
            Debug.Log("hit");
            SendMessage(CurrentId, MessageText);
        }

        if (OtherId > -1)
        {
            StartCoroutine(MessagesHelper.SendMessage(AuthHelper.GetId(), AuthHelper.GetToken(), OtherId, SendText));
            OtherId = -1;
            SendText = "";
        }
        if (ReloadChat > -1)
        {
            StartCoroutine(MessagesHelper.GetMessagesList(AuthHelper.GetId(), AuthHelper.GetToken(), ReloadChat));
            ReloadChat = -1;
        }
        if (UpdateChat)
        {
            GameObject[] friendItemObjects = GameObject.FindGameObjectsWithTag("Message");
            GameObject chatbox = GameObject.Find("Chat Content");
            foreach (GameObject item in friendItemObjects)
            {
                Destroy(item);
            }
            foreach (Message message in Messages)
            {
                GameObject newSentMessage;
                if (message.SenderId == AuthHelper.GetId())
                {
                    newSentMessage = Instantiate(myMessagePrefab, chatbox.transform);
                }
                else
                {
                    newSentMessage = Instantiate(theirMessagePrefab, chatbox.transform);
                }
                TextMeshProUGUI content = newSentMessage.transform.Find("Message Text").gameObject.GetComponent<TextMeshProUGUI>();;
                content.text = message.Content;

                ContentSizeFitter csf = newSentMessage.GetComponent<ContentSizeFitter>();
                csf.enabled = false;
                csf.SetLayoutVertical();
                csf.enabled = true;
            }
            UpdateChat = false;
        }
    }

    public static void OpenChat(int id, string username)
    {
        CloseChat();
        if (ChatPrefabStatic != null)
        {
            GameObject canvas = GameObject.Find("Canvas");
            GameObject chatObject = Instantiate(ChatPrefabStatic, canvas.GetComponent<RectTransform>(), false);

            GameObject chatHeading = chatObject.transform.Find("Chat Heading").gameObject;
            TextMeshProUGUI partner = chatHeading.transform.Find("Chat Partner").gameObject.GetComponent<TextMeshProUGUI>();
            Button closeButton = chatHeading.transform.Find("Close").gameObject.GetComponent<Button>();
            partner.text = username;
            closeButton.onClick.AddListener(CloseChat);

            MessageText = chatObject.transform.Find("Chat Input").GetComponent<TMP_InputField>();
            CurrentId = id;
            Button sendButton = chatObject.transform.Find("Send").gameObject.GetComponent<Button>();
            sendButton.onClick.AddListener(delegate { SendMessage(id, MessageText); });

            ReloadChat = id;
        }
    }

    public static void CloseChat()
    {
        MessageText = null;
        CurrentId = -1;
        GameObject[] messagePanelObjects = GameObject.FindGameObjectsWithTag("Message Panel");
        foreach (GameObject item in messagePanelObjects)
        {
            Destroy(item);
        }
    }

    public static void SendMessage(int oid, TMP_InputField messageText)
    {
        if (messageText.text.Length > 0)
        {
            OtherId = oid;
            SendText = messageText.text;
            messageText.text = "";
        }
    }
}
