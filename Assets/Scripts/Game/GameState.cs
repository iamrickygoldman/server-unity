﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameState
{
    public static int MatchId {get; set;} = -1;
    public static int Player1Id {get; set;} = -1;
    public static int Player2Id {get; set;} = -1;
    public static string Player1Username {get; set;} = "";
    public static string Player2Username {get; set;} = "";

    public static void Clear()
    {
        MatchId = -1;
        Player1Id = -1;
        Player2Id = -1;
        Player1Username = "";
        Player2Username = "";
    }
}
