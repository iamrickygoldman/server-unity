﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Popup : MonoBehaviour
{
    public float duration = 3f;
    public float fadeInDuration = .25f;
    public float fadeOutDuration = .25f;

    private CanvasGroup Canvas {get; set;}
    private float CurrentAlpha {get; set;} = 0f;
    private float Counter {get; set;} = 0f;
    private bool FadingIn {get; set;} = false;
    private bool FadingOut {get; set;} = false;
    private bool Displayed {get; set;} = false;
    
    void Start()
    {
        Canvas = gameObject.GetComponent<CanvasGroup>();
        Canvas.alpha = CurrentAlpha;
        Canvas.blocksRaycasts = false;
    }

    void Update()
    {
        if (FadingIn)
        {
            if (fadeInDuration == 0f)
            {
                Counter = 0f;
                CurrentAlpha = 1f;
                Canvas.alpha = CurrentAlpha;
                FadingIn = false;
                Displayed = true;
                Canvas.blocksRaycasts = true;
            }
            else if (Counter < fadeInDuration)
            {
                Counter += Time.deltaTime;
                CurrentAlpha = Mathf.Lerp(0f, 1f, Counter / fadeInDuration);
                Canvas.alpha = CurrentAlpha;
            }
            else
            {
                Counter = 0f;
                FadingIn = false;
                Displayed = true;
                Canvas.blocksRaycasts = true;
            }
        }
        else if (Displayed)
        {
            if (duration == 0f)
            {
                
            }
            else if (Counter < duration)
            {
                Counter += Time.deltaTime;
            }
            else
            {
                Counter = 0f;
                Displayed = false;
                FadingOut = true;
            }
        }
        else if (FadingOut)
        {
            if (fadeOutDuration == 0f)
            {
                Counter = 0f;
                CurrentAlpha = 0f;
                Canvas.alpha = CurrentAlpha;
                FadingOut = false;
                Displayed = false;
                Canvas.blocksRaycasts = false;
            }
            if (Counter < fadeOutDuration)
            {
                Counter += Time.deltaTime;
                CurrentAlpha = Mathf.Lerp(1f, 0f, Counter / fadeInDuration);
                Canvas.alpha = CurrentAlpha;
            }
            else
            {
                Counter = 0f;
                FadingOut = false;
                Canvas.blocksRaycasts = false;
                Displayed = false;
            }
        }
    }

    public void Display()
    {
        if (!Displayed)
        {
            Displayed = true;
            FadingIn = true;
        }
    }

    public void Hide()
    {
        if (Displayed)
        {
            Displayed = false;
            FadingOut = true;
        }
    }
}
